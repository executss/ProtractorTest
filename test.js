describe('Protractor Demo App', function() {
    var firstNumber = element(by.model('first'));
    var secondNumber = element(by.model('second'));
    var operator = element(by.model('operator'));
    var goButton = element(by.id('gobutton'));
    var latestResult = element(by.binding('latest'));
    
    function checkResult(equal){
        expect(latestResult.getText()).toEqual(equal);
    };
    function operation(a, b, operation) {
     switch(operation){
        case '+':
            operation = 'ADDITION';
            break;
        case '/':
            operation = 'DIVISION';
            break;
        case '%':
            operation = 'MODULO';
            break;
        case '*':
            operation = 'MULTIPLICATION';
            break;
        case '-':
            operation = 'SUBTRACTION';
            break;
        default:
            operation = 'ADDITION';
        };
        operator.$('[value=' + operation + ']').click();
        firstNumber.sendKeys(a);
        secondNumber.sendKeys(b);
        goButton.click();
    }
  
    beforeEach(function() {
      browser.get('http://juliemr.github.io/protractor-demo/');
    });
    //
      it('check for correct division on 0', function() {
        
      operation(4, 0,'/'); 
      checkResult('Infinity'); // right result
         
    });
});