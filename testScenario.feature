Feature: Testing protractor-demo site

    Background: 
        Given: I am on "http://juliemr.github.io/protractor-demo/"

    Scenario: Check for correct division on 0
        When I select "/" from the Operator field
        And I enter "4" into the firstField
        And I enter "0" into the secondField
        And I click on the Go! button
        Then result should contain "Infinity"